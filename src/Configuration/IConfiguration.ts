export interface IConfiguration {
  navitiaSncfToken: string;

  twitterConsumerKey: string;
  twitterConsumerSecret: string;
  twitterAccessToken: string;
  twitterAccessTokenSecret: string;

  cacheDirectory?: string;
  deleteCachedFilesAfterDays?: number;

  allowTweeting: boolean;
}
