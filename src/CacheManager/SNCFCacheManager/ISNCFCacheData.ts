import { INavitiaResponseDisruption } from "../../Navitia/INavitiaResponse";

export interface ISNCFCacheData {
  totalTrips: number;
  disruptionsData: INavitiaResponseDisruption[];
}
