import path from "path";
import { Configuration } from "../Configuration";
import { Constants } from "../Utils/Constants";

export class CacheManager {
  public static get cacheFolder(): string {
    const configuredCacheFolder = Configuration.configuration.cacheDirectory;

    if (configuredCacheFolder) {
      return path.resolve(configuredCacheFolder);
    }

    return path.resolve(Constants.DEFAULT_CACHE_DIRECTORY);
  }
}
