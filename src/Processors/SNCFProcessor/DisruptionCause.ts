export class DisruptionCause {
  public cause: string;
  public occurrences: number = 0;
  public totalDelayedTrips: number = 0;
  public totalPartialTrips: number = 0;
  public totalCanceledTrips: number = 0;

  constructor(cause: string) {
    this.cause = cause;
  }

  public toString(): string {
    const stats = [];
    if (this.totalDelayedTrips > 0) {
      stats.push(`${this.totalDelayedTrips} ⏰`);
    }
    if (this.totalPartialTrips > 0) {
      stats.push(`${this.totalPartialTrips} ❗`);
    }
    if (this.totalCanceledTrips > 0) {
      stats.push(`${this.totalCanceledTrips} 🗑️ `);
    }

    let humanizedCause = `${this.cause}`;
    if (stats.length > 0) {
      humanizedCause += ` (${stats.join(", ")})`;
    }

    return humanizedCause;
  }
}
