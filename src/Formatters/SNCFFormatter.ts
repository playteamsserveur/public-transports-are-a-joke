import { SNCFProcessor } from "../Processors/SNCFProcessor";
import { Constants } from "../Utils/Constants";
import { EmojiHelper } from "../Utils/EmojiHelper";
import { MomentHelper } from "../Utils/MomentHelper";

export class SNCFFormatter {
  private _processor: SNCFProcessor;

  constructor(processor: SNCFProcessor) {
    this._processor = processor;
  }

  public formatDisruptions(): string {
    if (!this._processor.processed) {
      throw new Error("Cannot format disruptions : data not available.");
    }

    const title = this.formatDisruptionsTitle();
    const stats = [
      `🚆 Nombre de voyages : ${this._processor.totalTrips}`,
      `⏰ Trains en retards : ${this._processor.totalDelayedTrips} (${this._processor.delayedTripsPercentage}%)`,
      `❗ Services partiels : ${this._processor.totalPartialTrips} (${this._processor.partialTripsPercentage}%)`,
      `🗑️ Trains supprimés : ${this._processor.totalCanceledTrips} (${this._processor.canceledTripsPercentage}%)`,
      `⌚ Retard cumulé : ${MomentHelper.humanizeDuration(this._processor.totalDelay, "seconds")}`,
      this.formatDisruptionsFooter(),
    ];

    return `${title}\n${stats.join("\n")}`;
  }

  public formatDisruptionCauses(): string {
    if (!this._processor.processed) {
      throw new Error("Cannot format disruption causes : data not available.");
    }

    // Build the causes tweet line by line, starting with the title.
    const humanizedCauses: string[] = [
      `Les bonnes causes :\n`,
    ];

    let ranking = 1;
    let totalLength = humanizedCauses[0].length; // Adding the title as initial content.
    const maxLength = Constants.TWITTER_MAX_TWEET_LENGTH;
    for (const it of this._processor.causes.entries()) {
      const [key, value] = it;

      const humanizedCause = `${EmojiHelper.numberToEmojiString(ranking)} ${value.toString()}`;
      // If this line will exceed the max tweet length, stop there.
      if ((totalLength + humanizedCause.length + 1) > maxLength) {
        break;
      }
      humanizedCauses.push(humanizedCause + "\n");
      totalLength += humanizedCause.length + 1; // Add 1 for the newline char.
      ranking++;
    }

    return humanizedCauses.join("");
  }

  public formatDisruptionStops(): string {
    if (!this._processor.processed) {
      throw new Error("Cannot format disruption causes : data not available.");
    }

    // Build the stops tweet line by line, starting with the title.
    const humanizedStops: string[] = [
      `Les salles d'attente :\n`,
    ];

    let ranking = 1;
    let totalLength = humanizedStops[0].length; // Adding the title as initial content.
    const maxLength = Constants.TWITTER_MAX_TWEET_LENGTH;
    for (const it of this._processor.stops.entries()) {
      const [key, value] = it;

      const humanizedStop = `${EmojiHelper.numberToEmojiString(ranking)} ${value.toString()}`;
      // If this line will exceed the max tweet length, stop there.
      if ((totalLength + humanizedStop.length + 1) > maxLength) {
        break;
      }
      humanizedStops.push(humanizedStop + "\n");
      totalLength += humanizedStop.length + 1; // Add 1 for the newline char.
      ranking++;
    }

    return humanizedStops.join("");
  }

  private formatDisruptionsTitle(): string {
    const startDate = this._processor.startDate;
    const endDate = this._processor.endDate;

    if (MomentHelper.isSameDay(startDate, endDate)) {
      return `Le ${startDate.format("dddd DD MMMM YYYY")} :`;
    } else if (MomentHelper.isSameWeek(startDate, endDate)) {
      if (!MomentHelper.isSameYear(startDate, endDate)) {
        return `🔎 Récap' de la semaine du ${startDate.format("DD MMMM YYYY")} au ${endDate.format("DD MMMM YYYY")} :`;
      } else if (!MomentHelper.isSameMonth(startDate, endDate)) {
        return `🔎 Récap' de la semaine du ${startDate.format("DD MMMM")} au ${endDate.format("DD MMMM YYYY")} :`;
      } else {
        return `🔎 Récap' de la semaine du ${startDate.format("DD")} au ${endDate.format("DD MMMM YYYY")} :`;
      }
    } else {
      return `🔎 Big récap' du mois de ${startDate.format("MMMM YYYY")} :`;
    }
  }

  private formatDisruptionsFooter(): string {
    const startDate = this._processor.startDate;
    const endDate = this._processor.endDate;

    if (MomentHelper.isSameDay(startDate, endDate)) {
      return `⬇️ Déroulez pour découvrir les lauréats ! ⬇️`;
    } else {
      return `⬇️`;
    }
  }
}
