import path from "path";

export class Constants {
  public static TWITTER_MAX_TWEET_LENGTH = 265; // Removed 15 chars in case a username is prepended to the Tweet.

  public static DEFAULT_CACHE_DIRECTORY = path.resolve("./cache/");
  public static DEFAULT_CONFIG_FILE = path.resolve("./config.json");
}
