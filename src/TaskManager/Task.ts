import { IArgv } from "../Utils/ArgvHelper";

export abstract class Task {
  private _args: IArgv;

  constructor(args: IArgv) {
    this._args = args;
  }

  public abstract async execute(): Promise<void>;
}
