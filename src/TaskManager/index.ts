import { IArgv } from "../Utils/ArgvHelper";
import { CleanupCacheTask } from "./CleanupCacheTask";
import { TweetSNCFStatsTask } from "./TweetSNCFStatsTask";

export enum TaskGroup {
  TASK_GROUP_SNCF = "sncf",
}

export class TaskManager {
  public static async execute(taskGroup: TaskGroup, args: IArgv) {
    const tasks = this.TASKS_BY_GROUP[taskGroup];
    if (!tasks) {
      throw new Error(`No tasks found for given task group '${taskGroup}'.`);
    }

    for (const task of tasks) {
      await new (task)(args).execute();
    }
  }

  private static TASKS_BY_GROUP = {
    [TaskGroup.TASK_GROUP_SNCF]: [
      TweetSNCFStatsTask,
      CleanupCacheTask,
    ],
  };
}
